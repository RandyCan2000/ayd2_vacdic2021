const express = require('express')
const mongoose = require('mongoose')
const app = express()
const port = 3000

async function connect() {

    // Make Connection
    await mongoose.connect('mongodb://mongodb:27017/ht1Ayd2', {

      useNewUrlParser: true,
      useUnifiedTopology: true,
    
    });
    
    console.log('\nDatabase Connected :D\n');
};

app.get('/', (req, res) => {
  res.json(
      {
          name:"Randy Alexander Can Ajuchan",
          carnet:"201801527",
          ConnectDBmongo:"Connectado :)"
      }
  )
})

connect();
app.listen(port, () => {
  console.log(`Server listening at http://localhost:${port}`)
})